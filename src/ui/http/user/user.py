from fastapi import APIRouter
from src.user.repository.user_repository import UserRepository
from src.app.value_object.id_value_object import Id
from src.app.value_object.name_value_object import Name
from src.app.value_object.description_value_object import Description
from src.user.model.user.user import User
from pydantic import BaseModel
from typing import Optional

router = APIRouter()
userRepository = UserRepository()


class UserDto(BaseModel):
    name: str
    description: Optional[str] = None


@router.post("/user")
def create(userDto: UserDto):
    user = userRepository.add(
        User(Id(4), Name(userDto.name), None if userDto.description is None else Description(userDto.description)))

    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value
    }


@router.get("/user")
def index() -> list:
    users = userRepository.list()
    return list(
        map(
            lambda user:
            {
                'id': user.id.value,
                'name': user.name.value,
                'description': None if user.description is None else user.description.value
            }, users
        )
    )


@router.get("/user/{id}")
def view(id: int):
    user = userRepository.get(Id(id))
    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value
    }


@router.patch("/user/{id}")
def update(id: int, userDto: UserDto):
    user = userRepository.get(Id(id))
    if userDto.name is not None:
        user.name = Name(userDto.name)
    user.description = None if userDto.description is None else Description(userDto.description)
    userRepository.update()
    return {
        'id': user.id.value,
        'name': user.name.value,
        'description': None if user.description is None else user.description.value
    }


@router.delete("/user/{id}")
def delete(id):
    return {"Hello": "World"}
